## imports

source utils.sh

## required environment variables

if [[ -z "$GCP_PROJECT_ID" ]]; then
  error_and_exit "\$GCP_PROJECT_ID is not set" "Did you setup a service account?"
fi

if [[ -z "$GCP_SERVICE_ACCOUNT" ]]; then
  error_and_exit "\$GCP_SERVICE_ACCOUNT is not set" "Did you setup a service account?"
fi

if [[ -z "$GCP_SERVICE_ACCOUNT_KEY" ]]; then
  error_and_exit "\$GCP_SERVICE_ACCOUNT_KEY is not set" "Did you setup a service account?"
fi

## google cloud auth and configure

google_cloud_config

## set $INPUT_FOLDER if null

if [[ -z "$INPUT_FOLDER" ]]; then
  INPUT_FOLDER="/Inputs/"
fi

## iterate over all image files in $INPUT_FOLDER

for FILE in "$INPUT_FOLDER"/*.{jpg,png,jpeg,png}; do
  [ ! -f "$FILE" ] && echo "🟧 $FILE not found" continue  # skip iteration if $FILE does not exist
  echo "🟩 $FILE found"
  echo "Executing: gcloud ml vision detect-text $FILE > \"$FILE.detect-text.json\""
  gcloud ml vision detect-text $FILE > "$FILE.detect-text.json"
  sleep 0.5
done
